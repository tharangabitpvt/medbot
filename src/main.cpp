#include <Arduino.h>
#include <analogWrite.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <stdlib.h>
#include <EEPROM.h>

#define EEPROM_SIZE 3
#define CHN       10 

// Replace with your network credentials
const char* ssid     = "TR-House-Work";
const char* password = "34JGD8LMR2Q";
//const char* ssid     = "001D73908597";
//const char* password = "jtugkgtcmc143";
//const char* ssid     = "KUMARA";
//const char* password = "NDRM9EY2HQN";

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String mLFState = "off";
String mLBState = "off";
String mRFState = "off";
String mRBState = "off";
String turnFLeftState = "off";
String turnFRightState = "off";

String forwardOrBackward = "f";

String appKey = "";

// Assign output variables to GPIO pins
const int mLB = 15;
const int mLF = 2;
const int mRB = 4;
const int mRF = 16;
const int piezoPin = 5;

int forwardPWM = 150;
int reversePWM = 150;

String authKey = "2sMUxEN1xF";

int turndPWMForward = 120;
float turnPWDBackward = 120;

int EEPROM_ADD_FPWM = 0;
int EEPROM_ADD_RPWM = 1; 
int EEPROM_ADD_TURN_PWM_F = 2;
int EEPROM_ADD_TURN_PWM_B = 3;
int EEPROM_ADD_APPKEY = 4;

int buzzerFrequency = 2000;
int buzzerToneFrequency = 3000;
double buzzDutyLow = 125;
double buzzDutyHigh = 255;
double resolution = 8;

// Set your Static IP address
IPAddress local_IP(192, 168, 8, 126);
// Set your Gateway IP address
IPAddress gateway(192, 168, 8, 1);

IPAddress subnet(255, 255, 0, 0);

AsyncWebServer server(80);
AsyncWebSocket ws("/ws"); // access at ws://[esp ip]/ws
AsyncEventSource events("/events"); // event source (Server-Sent events)

//flag to use from web update to reboot the ESP
bool shouldReboot = false;
bool runContinuousBeep = false;

void onBadRequest(AsyncWebServerRequest *request){
  //Handle Unknown Request
  request->send(404);
}

void onBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total){
  //Handle body
}

void onUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final){
  //Handle upload
}

void onEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  //Handle WebSocket event
}

boolean validateRequest(AsyncWebServerRequest *request) {
      if(request->hasHeader("MDAuthCheck")) {
        AsyncWebHeader* h = request->getHeader("MDAuthCheck");
        String headerValue = h->value().c_str();
        return (headerValue != "" && headerValue == authKey);
      } else {
        return false;
      }
}

void stopBot() {
    runContinuousBeep = false;

    analogWrite(mLF, LOW);
    analogWrite(mLB, LOW);
    analogWrite(mRF, LOW);
    analogWrite(mRB, LOW);
}


void setup() {
  Serial.begin(115200);

  ledcSetup(CHN, buzzerFrequency, resolution);
  ledcAttachPin(piezoPin, CHN);

  EEPROM.begin(EEPROM_SIZE);

  // store setting values
  int fPwmEEPROM = EEPROM.readInt(EEPROM_ADD_FPWM);
  int rPWMEEPROM = EEPROM.readInt(EEPROM_ADD_RPWM);
  int turnPwmForwardEEPROM = EEPROM.readFloat(EEPROM_ADD_TURN_PWM_F);
  int turnPwmBackwardEEPROM = EEPROM.readFloat(EEPROM_ADD_TURN_PWM_B);
  String appKeyEEPROM = EEPROM.readString(EEPROM_ADD_APPKEY);

  if(fPwmEEPROM > 0) {
    forwardPWM = fPwmEEPROM;
  }

  if(rPWMEEPROM > 0) {
    reversePWM = rPWMEEPROM;
  }

  if(turnPwmForwardEEPROM > 0) {
    turndPWMForward = turnPwmForwardEEPROM;
  }

  if(turnPwmBackwardEEPROM > 0) {
    turnPWDBackward = turnPwmBackwardEEPROM;
  }

  if(appKeyEEPROM != "") {
    appKey = appKeyEEPROM;
  }


  // Initialize the output variables as outputs
  pinMode(mLF, OUTPUT);
  pinMode(mLB, OUTPUT);
  pinMode(mRF, OUTPUT);
  pinMode(mRB, OUTPUT);
  pinMode(piezoPin, OUTPUT);
  
  // Set outputs to LOW
  analogWrite(mLF, LOW);
  analogWrite(mLB, LOW);
  analogWrite(mRF, LOW);
  analogWrite(mRB, LOW);

  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // attach AsyncWebSocket
  ws.onEvent(onEvent);
  server.addHandler(&ws);

  // attach AsyncEventSource
  server.addHandler(&events);

  // init communication between Android and MCU
  server.on("/con_check", HTTP_GET, [](AsyncWebServerRequest *request){

    if(validateRequest(request)) {
      request->send(200, "text/plain", "con success");
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }

  });

  // pairing controller and MCU
  server.on("/pair_controller", HTTP_GET, [](AsyncWebServerRequest *request){
    
    if(validateRequest(request)) {
      if (request->hasParam("admin_auth")) {
        if(request->getParam("admin_auth")->value() == "yes") {
          appKey = request->getParam("app_key")->value();
        }
      } else {
        if(appKey != NULL && appKey != "" && appKey != request->getParam("app_key")->value()) {
          request->send(200, "text/plain", "already paired");
        } else {
          if (request->hasParam("app_key")) {
            appKey = request->getParam("app_key")->value();
            request->send(200, "text/plain", "pairing success");
            ledcWrite(CHN, buzzDutyLow);
            ledcWriteTone(CHN, buzzerToneFrequency);
            delay(300);
            ledcWriteTone(CHN,0);
            delay(300);
            ledcWriteTone(CHN, buzzerToneFrequency);
            delay(300);
            ledcWriteTone(CHN,0);
          } else {
            request->send(200, "text/plain", "key not found");
          }
        }
      }
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }
    
    //request->send(200, "text/plain", "con success");
  });

  // set PWM values
  server.on("/save_settings", HTTP_GET, [](AsyncWebServerRequest *request){

    if(validateRequest(request)) {
      if (request->hasParam("fpwm")) {
          forwardPWM = strtol(request->getParam("fpwm")->value().c_str(), NULL, 10);
          EEPROM.writeInt(EEPROM_ADD_FPWM, forwardPWM);
          EEPROM.commit();
      }
      if (request->hasParam("bpwm")) {
          reversePWM = strtol(request->getParam("bpwm")->value().c_str(), NULL, 10);
          EEPROM.writeInt(EEPROM_ADD_RPWM, reversePWM);
          EEPROM.commit();
      }
      if (request->hasParam("turn_pwm_f")) {
          turndPWMForward = strtol(request->getParam("turn_pwm_f")->value().c_str(), NULL, 10);
          EEPROM.writeInt(EEPROM_ADD_TURN_PWM_F, turndPWMForward);
          EEPROM.commit();
      }
      if (request->hasParam("turn_pwm_b")) {
          turnPWDBackward = strtol(request->getParam("turn_pwm_b")->value().c_str(), NULL, 10);
          EEPROM.writeInt(EEPROM_ADD_TURN_PWM_B, turnPWDBackward);
          EEPROM.commit();
      }

      request->send(200, "text/plain", "pwm set success");
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }

  });

  // go forward
  server.on("/go_forward", HTTP_GET, [](AsyncWebServerRequest *request){
    Serial.println("Go forward");
    forwardOrBackward = "f";
    
    if(validateRequest(request)) {
      analogWrite(mLF, forwardPWM);
      analogWrite(mRF, forwardPWM);
      analogWrite(mLB, LOW);
      analogWrite(mRB, LOW);

      runContinuousBeep = true;
      
      request->send(200, "text/plain", "succeeded");
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }

  });

  // go backward
  server.on("/go_backward", HTTP_GET, [](AsyncWebServerRequest *request){
    Serial.println("Go backward");
    forwardOrBackward = "b";

    if(validateRequest(request)) {
      analogWrite(mLF, LOW);
      analogWrite(mRF, LOW);
      analogWrite(mLB, reversePWM);
      analogWrite(mRB, reversePWM);

      runContinuousBeep = true;

      request->send(200);
      
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }
    
  });

  // turn left
  server.on("/turn_left", HTTP_GET, [](AsyncWebServerRequest *request){
    Serial.println("Turning left");
    
    if(validateRequest(request)) {
      if(request->hasParam("smooth_turn") && request->getParam("smooth_turn")->value() == "true") {
        analogWrite(mLF, turnPWDBackward);
        analogWrite(mLB, LOW);
        analogWrite(mRF, turndPWMForward);
        analogWrite(mRB, LOW);
      } else {
        analogWrite(mLF, LOW);
        analogWrite(mLB, LOW);
        analogWrite(mRF, forwardPWM);
        analogWrite(mRB, LOW);
      }
      
      runContinuousBeep = true;

      request->send(200);  
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }
    
  });

  // turn right
  server.on("/turn_right", HTTP_GET, [](AsyncWebServerRequest *request){
    Serial.println("Turning right");
    mLBState = "off";

    if(validateRequest(request)) {
      if(request->hasParam("smooth_turn") && request->getParam("smooth_turn")->value() == "true") {
        analogWrite(mLF, turndPWMForward);
        analogWrite(mLB, LOW);
        analogWrite(mRF, turnPWDBackward);
        analogWrite(mRB, LOW);
      } else {
        analogWrite(mLF, forwardPWM);
        analogWrite(mLB, LOW);
        analogWrite(mRF, LOW);
        analogWrite(mRB, LOW);

      }
      
      runContinuousBeep = true;

      request->send(200);
      
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }

  });

// stop bot
  server.on("/stop", HTTP_GET, [](AsyncWebServerRequest *request) {
    Serial.println("Stop bot");
    
    stopBot();

    request->send(200);
  });

  // reboot MCU
  server.on("/reboot_mcu", HTTP_GET, [](AsyncWebServerRequest *request) {

    if(validateRequest(request)) {
      Serial.println("Reboot request");
      shouldReboot = true;

      request->send(200);
    } else {
      stopBot();
      request->send(401, "text/plain", "auth failed");
    }

  });

  // set buzzer frequency and dutycycle
    server.on("/buzzer", HTTP_GET, [](AsyncWebServerRequest *request) {
      buzzDutyHigh = strtol(request->getParam("duty")->value().c_str(), NULL, 10);
      buzzerToneFrequency = strtol(request->getParam("tone_freq")->value().c_str(), NULL, 10);

      request->send(200, "text/plain", "buzzer set");
    });

  // Catch-All Handlers
  // Any request that can not find a Handler that canHandle it
  // ends in the callbacks below.
  server.onNotFound(onBadRequest);
  server.onFileUpload(onUpload);
  server.onRequestBody(onBody);

  server.begin();
}

void loop() {
  if(shouldReboot){
    Serial.println("Rebooting...");
    delay(100);
    ESP.restart();
  }

   if(runContinuousBeep) {
     ledcWrite(CHN, buzzDutyHigh);
     ledcWriteTone(CHN, buzzerToneFrequency);
     delay(500);
     ledcWriteTone(CHN,0);
     delay(500);
   }
}